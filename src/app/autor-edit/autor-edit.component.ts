import { Component, OnInit } from '@angular/core';
import { Autor, AutorTableComponent } from './../autor-table/autor-table.component';

@Component({
  selector: 'autor-edit',
  templateUrl: './autor-edit.component.html',
  styles: ['']
})
export class AutorEditComponent implements OnInit {

  currentAutor: Autor = null;

  constructor(/*private autorTable: AutorTableComponent*/) { }

  ngOnInit() {
  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InputGroupComponent } from './input-group/input-group.component';
import { AutorTableComponent } from './autor-table/autor-table.component';
import { AutorEditComponent } from './autor-edit/autor-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    InputGroupComponent,
    AutorTableComponent,
    AutorEditComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

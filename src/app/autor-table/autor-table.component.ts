import { Component, OnInit } from '@angular/core';

export interface Autor{
  id: string;
  name: string;
}

@Component({
  selector: 'autor-table',
  templateUrl: './autor-table.component.html',
  styles: ['']
})
export class AutorTableComponent implements OnInit {
  autors: Autor[] = [{id: '0', name: "Anthony"}, {id: '1', name: "Javier"}];
  autor: Autor; 

  constructor() { }

  ngOnInit() {
  }

  getAutors(){
    return this.autors; 
  }
}


